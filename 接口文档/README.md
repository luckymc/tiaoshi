### 接口相关说明

### 统一出参结果

| 参数名称    | 类型    | 说明  |
| ---------   | -----   | ----- |
| respCode    | int     | 响应代码，0失败 1成功
| respMsg     | string  | 响应信息
| data        | mixed   | 响应数据
